const path = require('path');

module.exports = {
  entry: './src/main.ts',
  mode: 'development',
  watch: true,

  watchOptions: {
    poll: 2000,
  },

  output: {
    filename: 'html/phy-console.min.js',
    path: path.resolve(__dirname),
  },

  resolve: {
    extensions: ['.ts'],
  },

  module: {
    rules: [{
      test: /\.ts$/,
      use: 'ts-loader',
    }],
  },
};