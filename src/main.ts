import { input, output, print } from "./commands/command-ui";
import { canvas as mainc } from './rendering/main-canvas';
import { graphCanvas as graphc } from './rendering/graph';

document.addEventListener('DOMContentLoaded', () => {
  const top = document.getElementById('top');
  const bottom = document.getElementById('bottom');
  const left = document.getElementById('left');

  top.appendChild(output);
  bottom.appendChild(input);
  left.appendChild(mainc);
  left.appendChild(graphc);

  print('Enter <span class="syntax-name">start</span> to start the simulation.');
  print('Enter <span class="syntax-name">help</span> to display help.');
  input.focus();
});
