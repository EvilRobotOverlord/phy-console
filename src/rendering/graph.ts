import { Vector, sub } from "../objects/util/vector";

export const graphCanvas = document.createElement('canvas');
graphCanvas.height = 200;
graphCanvas.width = 800;

const ctx = graphCanvas.getContext('2d');

export type Graphable = (x: number) => number;

export interface GraphOptions {
  start: Vector;
  end: Vector;
}

export function graph(f: Graphable, options: GraphOptions) {
  ctx.beginPath();
  ctx.fillStyle = 'white';
  ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  ctx.fillStyle = 'black';
  ctx.lineWidth = 5;

  const bounds = sub(options.end, options.start);
  const inc = [bounds[0] / ctx.canvas.width, bounds[1] / ctx.canvas.height];

  if (options.start[0] <= 0 && options.end[0] >= 0) { // draw X axis if it's on the graph
    const xAxis = -inc[0] * options.start[0];
    ctx.beginPath();
    ctx.moveTo(xAxis, 0);
    ctx.lineTo(xAxis, 200);
    ctx.stroke();
  }

  if (options.start[1] <= 0 && options.end[1] >= 0) { // draw Y axis if it's on the graph
    const yAxis = -inc[1] * options.start[1];
    ctx.beginPath();
    ctx.moveTo(0, yAxis);
    ctx.lineTo(800, yAxis);
    ctx.stroke();
  }

  ctx.fillStyle = '#f44242';
  ctx.beginPath();
  ctx.moveTo(0, inc[1] * (f(options.start[0]) + options.start[1]));

  for (let i = 1; i < 800; i++) {
    ctx.lineTo(i, inc[1] * (f(options.start[0] + i * inc[0]) + options.start[1]));
  }

  ctx.stroke();
}
