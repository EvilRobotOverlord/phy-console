import { Energy } from "../energy-forms/energy";
import { names } from "../commands/command";
import { add, scale } from "../objects/util/vector";
import { EnergyReleaser } from "../objects/energy-releaser";

export const energies: Energy[] = [];

export const canvas = document.createElement('canvas');
canvas.height = 450;
canvas.width = 800;

const ctx = canvas.getContext('2d');

export const anim = {
  id: 0,
  running: false,
  prevDraw: 0,

  draw() {
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    const now = performance.now();
    const delta = now - anim.prevDraw;

    for (let i = energies.length - 1; i >= 0; i--) { // loop backwards because we have to delete some stuff too
      const energy = energies[i];

      if (energy.lifespan < now - energy.creation) {
        energies.splice(i, 1);
        console.log('removed energy');
        continue;
      }

      energy.update(now);
      energy.draw(ctx);
    }

    for (const [, particle] of names) {
      particle.pos = add(particle.pos, scale(particle.v, delta));
      particle.draw(ctx);

      if ('createEnergy' in particle) {
        (particle as EnergyReleaser).createEnergy(now);
      }
    }

    if (anim.running) {
      anim.id = requestAnimationFrame(anim.draw);
    }
  },

  start() {
    anim.running = true;
    anim.prevDraw = performance.now();
    anim.draw();
  },

  pause() {
    anim.running = false;
    cancelAnimationFrame(anim.id);
  },
};
