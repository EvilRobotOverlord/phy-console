import { Particle } from "../objects/particle";

export interface Argument {
  type: string;
  name: string;
}

export interface Command {
  perform(args: any[]): void;
  description: string;
  args: Argument[];
}

export function int(name: string): Argument {
  return {
    type: 'i',
    name,
  };
}

export function ident(name: string): Argument {
  return {
    type: 'd',
    name,
  };
}

export function str(name: string): Argument {
  return {
    type: 's',
    name,
  }
}

export const names = new Map<string, Particle>();
