import { Argument, names } from "./command";

export default function validateArgs(args: string[], types: Argument[]): any[] {
  if (args.length !== types.length) {
    throw new Error('The number of required arguments for the specified command is incorrect.');
  }

  const out = [];

  for (let i = 0; i < types.length; i++) {
    switch (types[i].type) {
      case 'i':
        const f = parseFloat(args[i]);

        if (isNaN(f))
          throw new Error(`The argument ${types[i].name} could not be parsed into a number.`);

        out.push(f);
        break;

      case 's':
        out.push(args[i]);
        break;

      case 'd':
        if (!names.has(args[i]))
          throw new Error(`The argument ${types[i].name} is not a valid identifier for any object created.`);

        out.push(args[i]);
        break;
    }
  }

  return out;
}
