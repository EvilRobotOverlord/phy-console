import { commands } from "./exports";
import { print } from "./command-ui";

export function syntaxName(command: string): string {
  return `<span class="syntax-name">${command}</span>`
}

export function argType(arg: string, type: string): string {
  return `<span class="type-${type}">&lt;${arg}&gt;</span>`;
}

export function printSyntax(name: string) {
  const cmd = commands[name];

  print(syntaxName(name), ': ', cmd.description);
  print('Syntax: ',
    syntaxName(name),
    ' ',
    cmd.args
      .map(
        (arg) => argType(arg.name, arg.type)
      )
      .join(' ')
  );
  print();
}
