import { call } from "./exports";

export const input = document.createElement('input');
input.type = 'text';

export const output = document.createElement('div');
output.id = 'output';

export function print(...args: string[]) {
  const text = [output.innerHTML];

  for (const arg of args) {
    text.push(arg);
  }

  text.push('<br>');
  output.innerHTML = text.join('');
  output.scrollTop = output.scrollHeight;
}

input.onkeydown = (e) => {
  if (e.code === 'Enter') {
    e.preventDefault();

    setTimeout(() => {
      const cmds = input.value.split('; ');
      print();
      print('> ', input.value);
      input.value = '';

      for (const cmd of cmds) {
        const args = cmd.split(' ');

        try {
          call(args.shift(), args);
        } catch (error) {
          print('<span class="error">', error.message, '</span>');
        }
      }
    }, 10);
  }
};
