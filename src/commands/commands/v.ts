import { Command, ident, int, names } from "../command";
import { print } from "../command-ui";
import { argType } from "../colour";

const v: Command = {
  description: 'Set the X and Y velocities of a specific particle',
  args: [ident('particle'), int('x'), int('y')],

  perform(args: any[]) {
    names.get(args[0]).v = [args[1] / 1000, args[2] / 1000];
    print('Set velocity of ', argType(args[0], 'd'));
  }
};

export default v;
