import { Command } from "../command";
import { anim } from "../../rendering/main-canvas";
import { print } from "../command-ui";

const pause: Command = {
  description: 'Pause the simulation',
  args: [],

  perform() {
    anim.pause();
    print('Paused simulation.');
  },
};

export default pause;
