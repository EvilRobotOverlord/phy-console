import { Command } from "../command";
import { output, print } from "../command-ui";

const clear: Command = {
  description: 'Pause the simulation',
  args: [],

  perform() {
    output.innerHTML = '';
    print('Cleared console.');
  },
};

export default clear;
