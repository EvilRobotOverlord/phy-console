import { Command, ident, int, names } from "../command";
import { emitter } from '../../objects/emitter';
import { print } from "../command-ui";
import { argType } from "../colour";

const emit: Command = {
  description: 'Set the radius of a specific particle',
  args: [ident('particle'), int('freuqency'), int('amplitude')],

  perform(args: any[]) {
    const p = emitter(names.get(args[0]), args[1] / 1000, args[2]);
    names.set(args[0], p);
    print('Started wave emission on ', argType(args[0], 'd'));
  }
};

export default emit;
