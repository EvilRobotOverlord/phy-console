import { Command, str } from "../command";
import { commands } from "../exports";
import { printSyntax } from "../colour";

const syntax: Command = {
  description: 'Pause the simulation',
  args: [str('command_name')],

  perform(args: any[]) {
    if (args[0] in commands) {
      printSyntax(args[0]);
    } else {
      throw new Error('Invalid command.');
    }
  },
};

export default syntax;
