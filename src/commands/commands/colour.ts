import { Command, ident, str, names } from "../command";
import { print } from "../command-ui";
import { argType } from "../colour";

const colour: Command = {
  description: 'Set the colour of a specific particle',
  args: [ident('particle'), str('colour')],

  perform(args: any[]) {
    names.get(args[0]).colour = args[1];
    print('Set colour of ', argType(args[0], 'd'));
  }
};

export default colour;
