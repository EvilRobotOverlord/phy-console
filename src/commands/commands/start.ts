import { Command } from "../command";
import { anim } from "../../rendering/main-canvas";
import { print } from "../command-ui";

const start: Command = {
  description: 'Start the simulation',
  args: [],

  perform() {
    anim.start();
    print('Started simulation.');
  },
};

export default start;
