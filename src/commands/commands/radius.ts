import { Command, ident, int, names } from "../command";
import { radius as r } from '../../objects/radius';
import { print } from "../command-ui";
import { argType } from "../colour";

const radius: Command = {
  description: 'Set the radius of a specific particle',
  args: [ident('particle'), int('radius')],

  perform(args: any[]) {
    names.set(args[0], r(names.get(args[0]), args[1]));
    print('Set radius of ', argType(args[0], 'd'));
  }
};

export default radius;
