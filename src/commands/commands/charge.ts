import { Command, ident, int, names } from "../command";
import { charge as c } from '../../objects/charge';
import { print } from "../command-ui";
import { argType } from "../colour";

const charge: Command = {
  description: 'Set the charge of a specific particle',
  args: [ident('particle'), int('charge')],

  perform(args: any[]) {
    const p = names.get(args[0]);
    names.set(args[0], c(p, args[1]));
    print('Charged ', argType(args[0], 'd'));
  }
};

export default charge;
