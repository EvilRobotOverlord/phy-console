import { Command, ident, int, names } from "../command";
import { mass as m } from '../../objects/mass';
import { print } from "../command-ui";
import { argType } from "../colour";

const mass: Command = {
  description: 'Set the mass of a specific particle',
  args: [ident('particle'), int('mass')],

  perform(args: any[]) {
    const p = names.get(args[0]);
    names.set(args[0], m(p, args[1]));

    print('Set mass of ', argType(args[0], 'd'));
  }
};

export default mass;
