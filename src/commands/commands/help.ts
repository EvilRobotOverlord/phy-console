import { Command } from "../command";
import { commands } from "../exports";
import { print } from "../command-ui";

const help: Command = {
  description: 'Show help.',
  args: [],

  perform() {
    print('Enter <span class="syntax-name">syntax</span> <span class="type-s">&lt;command_name&gt;</span> to get the syntax of a specific command.');
    print('Types are represented by colour:');
    print('<span class="type-s">string</span> <span class="type-i">number</span> <span class="type-d">identifier</span>');
    print();

    for (const key of Object.keys(commands)) {
      print('<span class="syntax-name">', key, '</span>');
    }
  },
};

export default help;
