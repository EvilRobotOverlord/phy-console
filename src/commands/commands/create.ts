import { Command, str, int, names } from "../command";
import { particle } from "../../objects/particle";
import { print } from "../command-ui";
import { argType } from "../colour";

const create: Command = {
  description: 'Create a new object with a name at the specified x and y co-ordinates',
  args: [str('identifier'), int('x'), int('y')],

  perform(args: any[]) {
    const p = particle([args[1], args[2]]);
    names.set(args[0], p);
    print('Created ', argType(args[0], 'd'));
  },
};

export default create;
