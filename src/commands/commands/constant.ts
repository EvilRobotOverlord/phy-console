import { Command, str, int } from "../command";
import { factors } from "../../factors";
import { print } from "../command-ui";
import { syntaxName } from "../colour";

const constant: Command = {
  description: 'Change the value of a constant.',
  args: [str('property_name'), int('new_value')],

  perform(args: any[]) {
    if (!(args[0] in factors))
      throw new Error(`${args[0]} is not a valid constant.`);

    factors[args[0]] = args[1];
    print('Changed the value of ', syntaxName(args[0]));
  }
};

export default constant;
