import create from "./commands/create";
import radius from "./commands/radius";
import validateArgs from "./validate-args";
import { Command } from "./command";
import charge from "./commands/charge";
import emit from "./commands/emit";
import mass from "./commands/mass";
import { print } from "./command-ui";
import pause from "./commands/pause";
import start from "./commands/start";
import syntax from "./commands/syntax";
import help from "./commands/help";
import clear from "./commands/clear";
import colour from "./commands/colour";
import v from "./commands/v";
import constant from "./commands/constant";

interface CommandList {
  [name: string]: Command;
}

export const commands: CommandList = {
  charge,
  clear,
  colour,
  constant,
  create,
  emit,
  help,
  mass,
  pause,
  radius,
  start,
  syntax,
  v,
};

export function call(name: string, args: string[]) {
  if (!(name in commands))
    throw new Error(`${name} is not a valid command.`);

  const cmd = commands[name];
  cmd.perform(validateArgs(args, cmd.args));
}
