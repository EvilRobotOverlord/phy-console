import { Particle } from "./particle";
import { Field } from "./field";
import { Vector, sub, length, scale } from "./util/vector";
import { factors } from "../factors";

export interface Charge extends Particle, Field {
  charge: number;
}

export function charge<T extends Particle>(p: T, charge: number): T & Charge {
  return {
    charge,
    ...p,

    getValue(pos: Vector): Vector {
      const dVec = sub(this.pos, pos);
      const d = length(dVec);
      const newtonsPerCoulomb = factors.coulombsConstant * this.charge / d ** 2;

      return scale(scale(dVec, 1 / d), newtonsPerCoulomb); // scale the unit vector along dVec by the value of the electrical field
    },
  };
}
