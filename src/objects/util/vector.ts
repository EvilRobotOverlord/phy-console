export type Vector = [number, number];

/**
 * Because TypeScript
 * @param a 
 */
export function vec(a: number[]): Vector {
  return [a[0], a[1]];
}

/**
 * Add two vectors
 * @param a 
 * @param b 
 */
export function add(a: Vector, b: Vector): Vector {
  return [a[0] + b[0], a[1] + b[1]];
}

/**
 * Subtract 2nd vector from first
 * @param a 
 * @param b 
 */
export function sub(a: Vector, b: Vector): Vector {
  return [a[0] - b[0], a[1] - b[1]];
}

/**
 * Scale vector
 * @param a 
 * @param k 
 */
export function scale(a: Vector, k: number): Vector {
  return [a[0] * k, a[1] * k];
}

/**
 * Convert polar vector to cartesian vector
 * @param theta 
 * @param radius 
 */
export function fromPolar(theta: number, radius: number): Vector {
  return [Math.cos(theta) * radius, Math.sin(theta) * radius]
}

/**
 * Returns the length of a vector
 * @param a 
 */
export function length(a: Vector): number {
  return Math.hypot(a[0], a[1]);
}
