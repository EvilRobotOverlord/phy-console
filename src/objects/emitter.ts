import { Particle } from "./particle";
import { EnergyReleaser } from "./energy-releaser";
import { Field } from "./field";
import { Vector } from "./util/vector";
import { factors } from "../factors";
import { energies } from "../rendering/main-canvas";
import { wave } from "../energy-forms/wave";

export enum EmitterStyle {
  Line,
  Circle,
  Wave,
}

export interface Emitter extends EnergyReleaser, Field {
  frequency: number,
  amplitude: number,
}

function getValue(p: Particle, f: number, a: number, pos: Vector, time: number): number {
  const d = Math.sqrt((p[0] - pos[0]) ** 2 + (p[1] - pos[1]) ** 2);

  if (d / factors.waveVelocity < (time - p.creation)) // if the wave has not reached yet
    return 0;

  return a * Math.sin(2 * Math.PI * f * (d - time) / factors.waveVelocity);
}

export function emitter<T extends Particle>(p: T, frequency: number, amplitude: number): T & Emitter {
  return {
    frequency,
    amplitude,
    lastEnergy: performance.now(),

    getValue(pos: Vector, time: number) {
      return getValue(p, frequency, amplitude, pos, time);
    },

    createEnergy(time: number) {
      if (time - this.lastEnergy > 1 / this.frequency) {
        energies.push(wave(this));
        this.lastEnergy = time;
        console.log('created wave');
      }
    },

    ...p,
  };
}
