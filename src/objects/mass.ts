import { Particle } from "./particle";
import { Field } from "./field";
import { Vector, sub, scale, length } from "./util/vector";
import { factors } from "../factors";

export interface Mass extends Particle, Field {
  mass: number;
}

export function mass<T extends Particle>(p: T, mass: number): T & Mass {
  return {
    mass,
    ...p,

    getValue(pos: Vector): Vector {
      const dVec = sub(this.pos, pos);
      const d = length(dVec);
      const newtonsPerKg = factors.newtonsGravitationalConstant * this.mass / d ** 2;

      return scale(scale(dVec, 1 / d), newtonsPerKg); // scale the unit vector along dVec by the value of the electrical field
    },
  };
}
