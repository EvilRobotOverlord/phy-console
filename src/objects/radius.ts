import { Particle } from "./particle";

export interface Radius extends Particle {
  radius: number;
}

export function radius<T extends Particle>(p: T, radius: number): T & Radius {
  return {
    radius,
    ...p,
    draw(ctx: CanvasRenderingContext2D) {
      ctx.beginPath();
      ctx.fillStyle = this.colour;
      ctx.arc(this.pos[0], this.pos[1], this.radius, 0, 2 * Math.PI);
      ctx.fill();
      console.log(this);
    }
  };
}
