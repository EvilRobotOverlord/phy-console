import { Particle } from "./particle";
import { Vector } from "./util/vector";

export interface Field extends Particle {
  getValue(pos: Vector, time: number): Vector | number;
}
