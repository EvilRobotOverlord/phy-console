import { Particle } from "./particle";

export interface EnergyReleaser extends Particle {
  lastEnergy: number;
  createEnergy: (t: number) => void;
}
