import { Vector, vec } from "./util/vector";

export interface Particle {
  v: Vector;
  pos: Vector;
  creation: number;
  colour: string;

  draw(ctx: CanvasRenderingContext2D): void;
}

export function particle(pos: Vector): Particle {
  return {
    v: vec([0, 0]),
    pos,
    creation: performance.now(),
    colour: 'black',

    draw(ctx: CanvasRenderingContext2D) {
      ctx.fillStyle = this.colour;
      ctx.fillRect(this.pos[0], this.pos[1], 10, 10);
    },
  };
}
