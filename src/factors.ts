export const factors = {
  waveVelocity: 1 / 16,
  coulombsConstant: 8.98755e9,
  newtonsGravitationalConstant: 6.67e-11,
};
