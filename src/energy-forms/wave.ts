import { Energy } from "./energy";
import { Particle } from "../objects/particle";
import { Vector } from "../objects/util/vector";
import { factors } from "../factors";

export interface Wave extends Energy {
  radius: number;
  origin: Vector;
}

export function wave(p: Particle): Wave {
  const obj = {
    owner: p,
    creation: performance.now(),
    radius: 0,
    origin: p.pos,
    lifespan: 918 / factors.waveVelocity,

    update(time: number) {
      obj.radius = (time - obj.creation) * factors.waveVelocity;
    },

    draw(ctx: CanvasRenderingContext2D) {
      ctx.strokeStyle = 'black';
      ctx.beginPath();
      ctx.arc(obj.origin[0], obj.origin[1], obj.radius, 0, 2 * Math.PI);
      ctx.stroke();
    },
  };

  return obj;
}
