import { Particle } from "../objects/particle";

/**
 * Representation of a form of energy (used only while rendering, not used in calculations)
 */
export interface Energy {
  owner: Particle;
  creation: number;
  draw(ctx: CanvasRenderingContext2D): void;
  update(time: number): void;
  lifespan: number;
}
